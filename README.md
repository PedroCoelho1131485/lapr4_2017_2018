# 1. Introduction

This project was develop by a class of 22 students for Laborátorio-Projeto IV course, divided in three groups (including me) of Computer Engineering in Instituto Superior de Engenharia do Porto (2016/2017).

This project aimed at building a web version of Cleansheets, a Desktop worksheet application available at [Bitbucket](https://bitbucket.org/lei-isep/csheets). 

Similarly to the desktop version, this new web version will use Java as the base programming language. The solution is based on GWT  [GWT Project](http://www.gwtproject.org).

"GWT is a development toolkit for building and optimizing complex browser-based applications. Its goal is to enable productive development of high-performance web applications without the developer having to be an expert in browser quirks, XMLHttpRequest, and JavaScript."

Below is presented a list with the instructions given by the Professors who planned this project :